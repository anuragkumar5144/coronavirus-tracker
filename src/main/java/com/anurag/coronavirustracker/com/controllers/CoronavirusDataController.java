package com.anurag.coronavirustracker.com.controllers;

import com.anurag.coronavirustracker.com.models.LocationStatsData;
import com.anurag.coronavirustracker.com.services.CoronavirusDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by anurag on 20/5/20.
 */
@Controller
public class CoronavirusDataController {

    @Autowired
    private CoronavirusDataService coronavirusDataService;

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("locationStats", coronavirusDataService.getAllStats());
        model.addAttribute("totalReportedCases", coronavirusDataService.getAllStats()
                .stream()
                .mapToInt(LocationStatsData::getTotalCount)
                .sum());
        model.addAttribute("totalCountPerDay", coronavirusDataService.getAllStats()
                .stream()
                .mapToInt(LocationStatsData::getPerDayCount)
                .sum());
        return "home";
    }
}
