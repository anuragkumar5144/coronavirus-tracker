package com.anurag.coronavirustracker.com.services;

import com.anurag.coronavirustracker.com.models.LocationStatsData;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anurag on 20/5/20.
 */

@Service
public class CoronavirusDataService {

    public static final String CORONAVIRUS_DATA_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";

    private RestTemplate restTemplate = new RestTemplate();

    List<LocationStatsData> allStats = new ArrayList<>();

    public List<LocationStatsData> getAllStats() {
        return allStats;
    }

    @PostConstruct
    @Scheduled(cron = "* * * 1 * *")
    public void fetchCoronavirusData() throws IOException {
        URI uri = UriComponentsBuilder.fromHttpUrl(CORONAVIRUS_DATA_URL).build().toUri();
        ResponseEntity<String> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(uri, HttpMethod.GET, null, String.class);
        } catch (HttpClientErrorException ex) {
            throw new HttpServerErrorException(ex.getStatusCode());
        }
        StringReader csvBodyReader = new StringReader(responseEntity.getBody());
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvBodyReader);
        List<LocationStatsData> locationStatsDataList = new ArrayList<>();
        for (CSVRecord record : records) {
            LocationStatsData locationStatsData = new LocationStatsData();
            locationStatsData.setState(record.get("Province/State"));
            locationStatsData.setCountry(record.get("Country/Region"));
            locationStatsData.setTotalCount(Integer.parseInt(record.get(record.size() - 1)));
            locationStatsData.setPerDayCount(locationStatsData.getTotalCount() - Integer.parseInt(record.get(record.size() - 2)));
            locationStatsDataList.add(locationStatsData);
        }
        this.allStats = locationStatsDataList;
    }
}
