package com.anurag.coronavirustracker.com.models;

/**
 * Created by anurag on 20/5/20.
 */
public class LocationStatsData {
    private String state;
    private String country;
    private int totalCount;
    private int perDayCount;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPerDayCount() {
        return perDayCount;
    }

    public void setPerDayCount(int perDayCount) {
        this.perDayCount = perDayCount;
    }

    @Override
    public String toString() {
        return "LocationStatsData{" +
                "state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", totalCount=" + totalCount +
                ", perDayCount=" + perDayCount +
                '}';
    }
}
